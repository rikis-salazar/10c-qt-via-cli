#include <QApplication>
// #include <QLabel>
// #include <QPushButton>

#include <QHBoxLayout>
#include <QSpinBox>
#include <QSlider>
// #include <QCurveBall>  // Joking :-P [ Let's go, Dodgers] Not joking ;-)

int main(int argc, char* argv[]){
    QApplication app(argc,argv);

    /**
        QLabel* label = 
            new QLabel("<h2><i>Hola</i>, <font color=red>Cu-te!</font></h2>");  
        label->show();
    **/

    /**
        QPushButton* button = new QPushButton("Salir");

        // Here, we "pass as parameters" the functions `clicked()` and `quit()` 
        QObject::connect(button, SIGNAL( clicked() ), &app, SLOT( quit() ) );

        button->show();
    **/

    // Here is our very first "real app"
    QWidget* window = new QWidget;
    window->setWindowTitle("Que tan viejo eres?");

    QSpinBox* spinbox = new QSpinBox();
    spinbox->setRange(0,127);
    // spinbox->setValue(31);  // <-- See below, past the `...::connect()` calls

    QSlider* slider = new QSlider(Qt::Horizontal);
    slider->setRange(0,127);


    // Let the "spinbox" and the "breaking ball" interact with each other
    QObject::connect(spinbox, SIGNAL( valueChanged(int) ),
                     slider, SLOT( setValue(int) ) );
    QObject::connect(slider, SIGNAL( valueChanged(int) ),
                     spinbox, SLOT( setValue(int) ) );

    // What changes if we set the value here???
    spinbox->setValue(31);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(spinbox);
    layout->addWidget(slider);

    window->setLayout(layout);
    window->show();

    return app.exec();   // <-- This part handles `signals` and `events`.
}
